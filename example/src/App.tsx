import React, { useEffect, useState, useCallback } from 'react'

import {
    Container,
    SearchFilter,
    SelectFilter,
    SortableHeader,
    PreviousPageButton,
    NextPageButton,
    TabularEntries,
    OnChangeArgs,
    PageSizeSelector,
    CurrentPageSelector,
    TotalPagesLabel,
    entries,
    fetchEntries,
    Pagination,
    ExampleEntry,
} from 'react-data-blocks'


const fEntries = fetchEntries(entries)
const fetchData = <T,>(args?: T): Promise<Pagination<ExampleEntry>> => new Promise<Pagination<ExampleEntry>>((resolve) => {
    const response = fEntries(args)
    setTimeout(() => resolve(response), 100)
})


export const RandomData: React.FC = () => {
    const [data, setData] = useState<Pagination<ExampleEntry>>()
    const onChange = useCallback((_args: OnChangeArgs): void => {
        console.log('RandomData::onChange', _args)
        fetchData(_args).then(r => setData(r))
    }, [])

    useEffect(() => {
        fetchData().then(r => setData(r))
    }, [])

    if (!data) {
        return <div>Loading...</div>
    }
    return (
        <Container data={data} onChange={onChange}>
            <div>
                <SearchFilter />
                <PageSizeSelector options={[10, 20, 30, 40, 50]} />
                <SelectFilter filterName="eventType" options={['CHECKIN', 'NEW_MEMBER']} />
            </div>

            <TabularEntries
                headers={[
                    {
                        render: <SortableHeader sortName="date">Date</SortableHeader>,
                        projection: (entry: ExampleEntry): React.ReactNode => entry?.date?.toISOString(),
                    },
                    {
                        render: <i>ID</i>,
                        // eslint-disable-next-line react/display-name
                        projection: (entry: ExampleEntry): React.ReactNode => <i>{entry.id}</i>,
                    },
                    {
                        render: <SortableHeader sortName="eventName">Event</SortableHeader>,
                        projection: (entry: ExampleEntry): React.ReactNode => entry.eventName,
                    },
                ]}
            />

            <div>
                <PreviousPageButton />
                <CurrentPageSelector /> / <TotalPagesLabel />
                {/* <CurrentPageLabel /> */}
                <NextPageButton />
            </div>
        </Container>
    )
}


const App: React.FC = () => {
    return (
        <>
            <RandomData />
        </>
    )
}

export default App
