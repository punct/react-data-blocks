import React, { useContext } from 'react'
import PropTypes from 'prop-types'

import { ATCContext } from './common'

export const PreviousPageButton: React.FC = () => {
    const { actions } = useContext(ATCContext)
    const onClick = (): void => {
        actions.previousPage()
    }
    return (
        <button onClick={onClick}>Previous</button>
    )
}


export const NextPageButton: React.FC = () => {
    const { actions } = useContext(ATCContext)
    const onClick = (): void => {
        actions.nextPage()
    }
    return (
        <button onClick={onClick}>Next</button>
    )
}


export const CurrentPageSelector: React.FC = () => {
    const { state, actions } = useContext(ATCContext)
    const onChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        try {
            const newPage = parseInt(event.target.value, 10)
            actions.setPage({ page: newPage - 1 })
        } catch (error) { return }
    }
    return (
        <input type="number" value={state.page + 1} onChange={onChange} />
    )
}


export const TotalPagesLabel: React.FC = () => {
    const { state } = useContext(ATCContext)
    return (
        <span>{Math.ceil(state.total / state.pageSize)}</span>
    )
}


export const PageSizeSelector: React.FC<{ options: number[] }> = (props) => {
    const { state, actions } = useContext(ATCContext)
    const onChange = (event: React.ChangeEvent<HTMLSelectElement>): void => {
        const newPageSize = parseInt(event.target.value, 10)
        actions.setPageSize({ pageSize: newPageSize })
    }
    return (
        <select onChange={onChange} value={state.pageSize}>
            {props.options.map(opt => <option key={opt} value={opt}>{opt}</option>)}
        </select>
    )
}
PageSizeSelector.propTypes = {
    options: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
}
