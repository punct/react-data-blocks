import * as faker from 'faker'

import { pipe } from './utils'


const gen = (l: number): number[] => [...Array(l)].map((_, i) => i)

export interface ExampleEntry {
    id: string;
    eventName: string;
    date: Date;
}

export const entries: ExampleEntry[] = gen(29).map(() => ({
    id: faker.random.uuid(),
    date: faker.date.past(),
    eventName: faker.name.jobTitle().replace(new RegExp(' ', 'g'), '_').toUpperCase(),
}))

type IPaginationFilter<T> = Partial<T>
// eslint-disable-next-line @typescript-eslint/no-explicit-any
interface PaginationOptionsFields<E extends { [key: string]: any }> {
    page: number;
    pageSize: number;
    sort: {
        name: keyof E;
        ascending: boolean;
    };
    filter: IPaginationFilter<E>;
}
type PaginationOptions<E> = Partial<PaginationOptionsFields<E>>;
export interface Pagination<E> {
    page: number;
    pageSize: number;
    total: number;

    results: E[];
}

const applyFilter = <T>(filters: IPaginationFilter<T> = {}): (entries: T[]) => T[] => {
    const f = Object.entries(filters)

    const filterFn = (entry: T): boolean => f.every(([name, value]) => {
        if (name === 'search') {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const matches = (entry as any)['eventName'].match(new RegExp(`${value}`, 'i'))
            return matches !== null
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        return (entry as any)[name] === value
    })
    return (entries: T[]): T[] => entries.filter(filterFn)
}

const compareNumbers = (a: number, b: number): number => {
    if (a < b) {
        return -1
    }
    if (a > b) {
        return 1
    }
    return 0
}
const localCompare = (a: unknown, b: unknown): number => {
    if (typeof a !== typeof b) {
        return -1
    }
    if (typeof a === 'string' && typeof b === 'string') {
        return a.toUpperCase().localeCompare(b.toUpperCase())
    }
    if (typeof a === 'number' && typeof b === 'number') {
        return compareNumbers(a, b)
    }
    if (typeof a === 'boolean') {
        if (a === false && b === true) {
            return -1
        }
        if (a === true && b === false) {
            return 1
        }
        return 0
    }
    if (typeof a === 'object' && typeof b === 'object') {
        if (a === null && b === null) {
            return 0
        }
        if (a === null) {
            return 1
        }
        if (b === null) {
            return -1
        }
        if (a.constructor !== b.constructor) {
            return -1
        }

        if (a instanceof Date && b instanceof Date) {
            return compareNumbers(a.getTime(), b.getTime())
        }
    }
    return -1
}
interface PaginationSort<T> {
    name: keyof T;
    ascending: boolean;
}
const applySort = <T>(sorts?: PaginationSort<T>) => (entries: T[]): T[] => {
    const compare = (a: T, b: T): number => {
        if (!sorts) {
            return -1
        }
        const result = localCompare(a[sorts.name], b[sorts.name])
        return sorts.ascending ? result : -result
    }
    return entries.sort(compare)
}

const applyPagination = (pageSize = 10, page = 0) => <T>(entries: T[]): T[] => {
    const start = pageSize * page
    const end = start + pageSize
    return entries.slice(start, end)
}

const defaultOptions: PaginationOptions<ExampleEntry> = {
    page: 0,
    pageSize: 10,
    sort: undefined,
    filter: {},
}
export const fetchEntries = (entries: ExampleEntry[]) => (options: PaginationOptions<ExampleEntry> = defaultOptions): Pagination<ExampleEntry> => {
    const page = options.page || 0
    const pageSize = options.pageSize || 10
    const total = entries.length

    const results = pipe(
        applyFilter(options.filter),
        applySort(options.sort),
        applyPagination(pageSize, page)
    )(entries)
    return {
        page,
        pageSize,
        total,
        results,
    }
}
