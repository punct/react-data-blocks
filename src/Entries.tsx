import React, { useContext } from 'react'
import PropTypes from 'prop-types'

import { ATCContext, ATCEntry } from './common'

import './Entries.css'

interface TabularEntriesProps {
    headers: {
        render: React.ReactNode;
        projection: (entry: ATCEntry) => React.ReactNode;
    }[];
}
export const TabularEntries: React.FC<TabularEntriesProps> = (props) => {
    const { data } = useContext(ATCContext)

    const entries = data.results || []
    return (
        <table>
            <thead>
                <tr>
                    {props.headers.map(header => <td key={Math.random()}>{header.render}</td>)}
                </tr>
            </thead>
            <tbody>
                {entries.map((entry) => {
                    return (
                        <tr key={Math.random()}>
                            {props.headers.map(header => <td key={Math.random()}>{header.projection(entry)}</td>)}
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}
TabularEntries.propTypes = {
    headers: PropTypes.arrayOf(
        PropTypes.shape({
            render: PropTypes.node.isRequired,
            projection: PropTypes.func.isRequired,
        }).isRequired
    ).isRequired,
}


interface SortableHeaderProps {
    sortName: string;
    children?: React.ReactNode;
}
export const SortableHeader: React.FC<SortableHeaderProps> = (props) => {
    const { state, actions } = useContext(ATCContext)
    const direction = (state.sort && state.sort.name === props.sortName)
        ? state.sort.ascending
            ? 'ASC'
            : 'DESC'
        : null

    const onClick = (): void => {
        actions.setSort({ name: props.sortName })
    }

    return (
        <div className='sortable-header' onClick={onClick}>
            <span className='sortable-header-text'>{props.children}</span>
            {direction}
        </div>
    )
}
SortableHeader.propTypes = {
    sortName: PropTypes.string.isRequired,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
}
