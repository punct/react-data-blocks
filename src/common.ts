import React from 'react'

import { Pagination } from './api'
import {
    AdvancedTableState,
    ClearFilterPayload,
    SetFilterPayload,
    SetPagePayload,
    SetPageSizePayload,
    SetSortPayload,
} from './state'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type ATCEntry = any

interface ATCActions {
    clearFilter: (args: ClearFilterPayload) => void;
    nextPage: () => void;
    previousPage: () => void;
    setFilter: (args: SetFilterPayload) => void;
    setPage: (args: SetPagePayload) => void;
    setPageSize: (args: SetPageSizePayload) => void;
    setSort: (args: SetSortPayload) => void;
}
type ATCContextShape = { state: AdvancedTableState; actions: ATCActions; data: Pagination<ATCEntry> }
export const ATCContext = React.createContext<ATCContextShape>({} as ATCContextShape)
