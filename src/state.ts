import { createAction, createReducer } from '@reduxjs/toolkit'

export interface SetFilterPayload { name: string; value: string }
export const setFilter = createAction<SetFilterPayload>('SET_FILTER')

export interface ClearFilterPayload { name: string }
export const clearFilter = createAction<ClearFilterPayload>('CLEAR_FILTER')

export interface SetSortPayload { name: string; ascending?: boolean }
export const setSort = createAction<SetSortPayload>('SET_SORT')

export interface SetPagePayload { page: number }
export const setPage = createAction<SetPagePayload>('SET_PAGE')

export type PreviousPagePayload = void
export const previousPage = createAction<PreviousPagePayload>('PREVIOUS_PAGE')

export type NextPagePayload = void
export const nextPage = createAction<NextPagePayload>('NEXT_PAGE')

export interface SetPageSizePayload { pageSize: number }
export const setPageSize = createAction<SetPageSizePayload>('SET_PAGE_SIZE')

export interface SetTotalPayload { total: number }
export const setTotal = createAction<SetTotalPayload>('SET_TOTAL')


export interface AdvancedTableState {
    page: number;
    pageSize: number;
    total: number;
    sort: {
        name: string;
        ascending: boolean;
    } | undefined;
    filter: {
        [name: string]: string;
    };
}

export const initialState: AdvancedTableState = {
    page: 0,
    pageSize: 10,
    total: 0,
    sort: undefined,
    filter: {},
}

export const reducer = createReducer(initialState, builder => builder
    .addCase(clearFilter, (state, action) => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { [action.payload.name]: _, otherFilters } = state.filter
            return {
            ...state,
            filter: otherFilters || {},
        }
    })
    .addCase(nextPage, (state) => ({
        ...state,
        page: Math.min(Math.ceil(state.total / state.pageSize) - 1, state.page + 1),
    }))
    .addCase(previousPage, (state) => ({
        ...state,
        page: Math.max(0, state.page - 1),
    }))
    .addCase(setFilter, (state, action) => ({
        ...state,
        filter: {
            ...state.filter,
            [action.payload.name]: action.payload.value,
        },
    }))
    .addCase(setPage, (state, action) => {
        const firstPage = 0
            const lastPage = Math.ceil(state.total / state.pageSize) - 1
            const indendedPage = action.payload.page
            return {
            ...state,
            page: Math.max(firstPage, Math.min(indendedPage, lastPage)),
        }
    })
    .addCase(setPageSize, (state, action) => ({
        ...state,
        pageSize: Math.max(0, action.payload.pageSize),
    }))
    .addCase(setSort, (state, action) => {
        const ascending = action.payload.ascending !== undefined
            ? action.payload.ascending
            : (state.sort && state.sort.name === action.payload.name)
                ? !state.sort.ascending
                : true
            const newSort = {
            name: action.payload.name,
            ascending,
        }
            return {
            ...state,
            sort: newSort,
        }
    })
    .addCase(setTotal, (state, action) => ({
        ...state,
        total: action.payload.total,
    }))
)
