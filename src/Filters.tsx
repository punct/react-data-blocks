import React, { useContext } from 'react'
import PropTypes from 'prop-types'

import { ATCContext } from './common'


export const TextFilter: React.FC<{ filterName: string }> = (props) => {
    const { state, actions } = useContext(ATCContext)
    const filter = state.filter[props.filterName]
    const onChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        actions.setFilter({ name: props.filterName, value: event.target.value })
    }
    return (
        <input type="text" value={filter} onChange={onChange} />
    )
}
TextFilter.propTypes = {
    filterName: PropTypes.string.isRequired,
}


export const SearchFilter: React.FC = () =>
    <TextFilter filterName="search" />


export const SelectFilter: React.FC<{ filterName: string; options: string[] }> = (props) => {
    const { state, actions } = useContext(ATCContext)
    const filter = state.filter[props.filterName]
    const onChange = (event: React.ChangeEvent<HTMLSelectElement>): void => {
        actions.setFilter({ name: props.filterName, value: event.target.value })
    }
    return (
        <select name={props.filterName} onChange={onChange} value={filter}>
            {props.options.map(opt => <option key={opt} value={opt}>{opt}</option>)}
        </select>
    )
}
SelectFilter.propTypes = {
    filterName: PropTypes.string.isRequired,
    options: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
}

