type Fn<A, B> = (args: A) => B

export const compose = <A, B, C>(a: Fn<A, B>, b: Fn<B, C>) => (arg: A): C => b(a(arg))

export function pipe<A, B, C>(
    fn1: Fn<A, B>,
    fn2: Fn<B, C>,
): Fn<A, C>;
export function pipe<A, B, C, D>(
    fn1: Fn<A, B>,
    fn2: Fn<B, C>,
    fn3: Fn<C, D>,
): Fn<A, D>;
export function pipe<A, B, C, D, E>(
    fn1: Fn<A, B>,
    fn2: Fn<B, C>,
    fn3: Fn<C, D>,
    fn4: Fn<D, E>,
): Fn<A, E>;
export function pipe<A, B, C, D, E, F>(
    fn1: Fn<A, B>,
    fn2: Fn<B, C>,
    fn3: Fn<C, D>,
    fn4: Fn<D, E>,
    fn5: Fn<E, F>,
): Fn<A, F>;
export function pipe<A, B, C, D, E, F, G>(
    fn1: Fn<A, B>,
    fn2: Fn<B, C>,
    fn3: Fn<C, D>,
    fn4: Fn<D, E>,
    fn5: Fn<E, F>,
    fn6: Fn<F, G>,
): Fn<A, G>;
export function pipe<A, B, C, D, E, F, G, H>(
    fn1: Fn<A, B>,
    fn2: Fn<B, C>,
    fn3: Fn<C, D>,
    fn4: Fn<D, E>,
    fn5: Fn<E, F>,
    fn6: Fn<F, G>,
    fn7: Fn<G, H>,
): Fn<A, H>;
export function pipe<A, B, C, D, E, F, G, H, I>(
    fn1: Fn<A, B>,
    fn2: Fn<B, C>,
    fn3: Fn<C, D>,
    fn4: Fn<D, E>,
    fn5: Fn<E, F>,
    fn6: Fn<F, G>,
    fn7: Fn<G, H>,
    fn8: Fn<H, I>,
): Fn<A, I>;
export function pipe<A, B, C, D, E, F, G, H, I, J>(
    fn1: Fn<A, B>,
    fn2: Fn<B, C>,
    fn3: Fn<C, D>,
    fn4: Fn<D, E>,
    fn5: Fn<E, F>,
    fn6: Fn<F, G>,
    fn7: Fn<G, H>,
    fn8: Fn<H, I>,
    fn9: Fn<I, J>,
): Fn<A, J>;
export function pipe(...ops: Function[]): Function {
    return ops.reduce((prevOp, nextOp) => compose(prevOp as never, nextOp as never))
}
