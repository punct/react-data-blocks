import React, { useEffect, useReducer } from 'react'
import PropTypes from 'prop-types'

import { Pagination } from './api'
import { ATCContext, ATCEntry } from './common'
import {
    AdvancedTableState,
    clearFilter,
    initialState,
    nextPage,
    previousPage,
    reducer,
    setFilter,
    setPage,
    setPageSize,
    setSort,
    setTotal,
    SetFilterPayload,
    ClearFilterPayload,
    SetSortPayload,
    SetPagePayload,
    SetPageSizePayload,
} from './state'

export type OnChangeArgs = Pick<AdvancedTableState, 'filter' | 'page' | 'pageSize' | 'sort'>

interface ContainerProps {
    data: Pagination<ATCEntry>;
    children: React.ReactNode;
    onChange?: (state: OnChangeArgs) => void;
}
export const Container: React.FC<ContainerProps> = (props) => {
    const { children, data, onChange } = props
    const [state, dispatch] = useReducer(reducer, initialState)

    const boundActions = {
        clearFilter: (args: ClearFilterPayload): void => dispatch(clearFilter(args)),
        nextPage: (): void => dispatch(nextPage()),
        previousPage: (): void => dispatch(previousPage()),
        setFilter: (args: SetFilterPayload): void => dispatch(setFilter(args)),
        setPage: (args: SetPagePayload): void => dispatch(setPage(args)),
        setPageSize: (args: SetPageSizePayload): void => dispatch(setPageSize(args)),
        setSort: (args: SetSortPayload): void => dispatch(setSort(args)),
    }

    useEffect(() => {
        dispatch(setTotal({ total: data.total }))
    }, [data.total])

    useEffect(() => {
        const newState = {
            page: state.page,
            pageSize: state.pageSize,
            sort: state.sort,
            filter: state.filter,
        }
        if (typeof onChange === 'function') {
            onChange(newState)
        }
    }, [onChange, state.page, state.pageSize, state.sort, state.filter])

    return (
        <ATCContext.Provider value={{ actions: boundActions, state, data }}>
            {children}
        </ATCContext.Provider>
    )
}
Container.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
    data: PropTypes.shape({
        page: PropTypes.number.isRequired,
        pageSize: PropTypes.number.isRequired,
        total: PropTypes.number.isRequired,
        results: PropTypes.array.isRequired,
    }).isRequired,
    onChange: PropTypes.func,
}
