# react-data-blocks

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/react-data-blocks.svg)](https://www.npmjs.com/package/react-data-blocks) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-data-blocks
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'react-data-blocks'
import 'react-data-blocks/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [](https://github.com/)
